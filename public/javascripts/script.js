$(document).ready(function() {
  var FilesToUpload = 1;

  // Load photos
  for(var i=0; i<30; i++) {
    $('#photos').append('<div class="element"></div>');
  }

  // Load albums
  for(var i=0; i<5; i++) {
    $('#albums').append('<div class="element"></div>');
  }

  // Uploader Script
  $('#OpenUploader').click(function() {
    $('#uploader').show();
  });

  $("#content").mouseup(function(e) {
      var subject = $("#uploader");
      if(e.target.id != subject.attr('id')) {
          subject.hide();
      }
  });

  $('#album_create').click(function() {
    if($('#album_create').prop( "checked" )) {
      $('#album_name').show();
    } else {
      $('#album_name').hide();
    }
  });

  $(document).on('click', '.remove', function() {
    if($('.upload_element').length == 1) {
      alert("Du kannst das letzte Element nicht entfernen");
    } else {
      $(this).parent().remove();
    }
  });

  $('#AddUploadElement').click(function() {
    FilesToUpload++;
    $('.upload_element').parent().last().append('<div class="upload_element"> <span class="remove">Entfernen</span><br><input type="text" placeholder="Name des Fotos" name="Upload_Name'+FilesToUpload+'"><br><input type="text" placeholder="Beschreibung (optional)" name="Upload_Desc'+FilesToUpload+'"><br><input name="Upload_File'+FilesToUpload+'" type="file"><br></div>');
  });

  $('#StartUpload').click(function() {
    $('#UploadForm').submit();
  });

  // Navigation Script
  $('.icon.home').click(function() {alert("Startseite")});
  $('.icon.photos').click(function() {alert("Fotos")});
  $('.icon.albums').click(function() {alert("Alben")});
  $('.icon.search').click(function() {
    $('#navigation .searchbar input').show();
  });

  // Element angeklickt
  $('.element').click(function() {
    alert("Element angeklickt");
  })

  $('.more').click(function() {
    alert("Mehr anzeigen");
  })
});
